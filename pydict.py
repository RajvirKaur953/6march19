# key:value pairs;mutable
student={             #dictionary: object
    'name':'Rajvir',
    'roll_no':1,
    'class':'python',
    'subjects':['HTML','CSS','PYTHON','JS'],
    'expenses':[10,0,20.5,1000,5000]
}
# print(student['name'])
# print(student['roll_no'])
# print(student['class'])
# print(type(student))
# print(type(student['roll_no']))
# print(student['subjects'][2])

for i in student:
    print(i, '=> ',student[i])

ex=student['expenses']
b=0
for i in ex:
    b+=i
print('Total Expenses:',b)
# student=[{
#     'name':'Rajvir',                #array
#     'roll_no':1,
#      'class':'python',
#      'subjects':['HTML','CSS','PYTHON','JS']
# }]
# print(student[0])