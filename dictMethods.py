d={}

# Add element in a dictionary
d['name']='Shinchan'
d['last_name']='Nohara'

d.update({'pet_name':'Sheero'})
d.update({'Age':5})

# update values in dictionary
d['name']='Harry'

# delete element
del d['pet_name']

# count number of elements
print(len(d))


# Search for a key
print('name' in d)
print('name' in d.keys())
print('Harry' in d.values())
#print(d.has_key('name'))


# Create a new dictionary from set of keys
t=('name','salary','age','leaves') 
newd=d.fromkeys(t,100)
# print(newd)

# fetch keys,values and pairs
print(d.keys())                                        #all the keys of dictionary
print(d.values())                                       #all the values of dictionary
print(d.items())                                        #all the items of dictionary
print(d)

# clear the dictionary
# print(d)
# d.clear()
# print(d)

# copy the dictionary
n=d.copy()
print(n)